import 'package:flutter/material.dart';
import 'package:ui_extention/checkbox_widget.dart';
import 'package:ui_extention/checkboxtile_widget.dart';
import 'package:ui_extention/dropdown_widget.dart';
import 'package:ui_extention/radio_widget.dart';

void main() {
  runApp(MaterialApp(
    title: 'Ui Extension', home: MyApp()
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key:  key);

  @override
  Widget build(BuildContext context) {
   return Scaffold(
       appBar: AppBar(title:  Text('Ui Extention')),
       drawer: Drawer(child: ListView(
         children: [
           GestureDetector(
             child: DrawerHeader(child: Text('Ui Menu'),
             decoration:  BoxDecoration(color: Colors.blue),
           ),
            onTap: (){
              Navigator.pop(context);
            },
           ),
           ListTile(
           title: Text('CheckBox'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => CheckBoxWidget()
             ));
           },
         ),
         ListTile(
           title: Text('CheckBoxTile'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => CheckBoxTileWidget()
             ));
           },
         ),
          ListTile(
           title: Text('DropDown'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => DropDownWidget()
             ));
           },
         ),
          ListTile(
           title: Text('Radio'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => RadioWidget()
             ));
           },
         ),
         ],
       ),),
       body: ListView(children: [
         ListTile(
           title: Text('CheckBox'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => CheckBoxWidget()
             ));
           },
         ),
         ListTile(
           title: Text('CheckBoxTile'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => CheckBoxTileWidget()
             ));
           },
         ),
          ListTile(
           title: Text('DropDown'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => DropDownWidget()
             ));
           },
         ),
          ListTile(
           title: Text('Radio'),
           onTap:  (){
             Navigator.push(context, 
             MaterialPageRoute(
               builder: (context) => RadioWidget()
             ));
           },
         ),
       ],),
        );
   
  }
}