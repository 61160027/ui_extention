import 'package:flutter/material.dart';

class RadioWidget extends StatefulWidget {
  RadioWidget({Key? key}) : super(key: key);

  @override
  _RadioWidgetState createState() => _RadioWidgetState();
}

class _RadioWidgetState extends State<RadioWidget> {
  int val = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Radio'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Radio(
              groupValue: val,
              value: 1,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('One'),
          ),
           ListTile(
            leading: Radio(
              groupValue: val,
              value: 2,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('Two'),
          ),
           ListTile(
            leading: Radio(
              groupValue: val,
              value: 3,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('Three'),
          ),
          TextButton.icon(
            icon: Icon(Icons.save), 
            label: Text('Save'),
            onPressed: (){
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Select $val')));
            },
          )
        ],
      ),
    );
  }
}
